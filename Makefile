
#all:
	#gcc -Os -std=c++11 -fno-strict-aliasing -fexceptions -Wall -Wextra -Wno-unused-parameter -Werror -o silecs-opcua-test -Iinclude silecs-test-opcua.cpp -L/user/mpopeang/git/silecs-comm-opcua-test/lib/L867 -lsilecs-comm -L/user/mpopeang/git/silecs-comm-opcua-test/libmodbus/3.1.6/lib -lmodbus -L/user/mpopeang/git/silecs-comm-opcua-test/snap7/1.4.2/build/bin/x86_64-linux -lsnap7 -L/user/mpopeang/git/silecs-comm-opcua-test/open62541/lib -lopen62541-compat -lstdc++ -lm -lrt -lpthread

SILECS_VERSION?=1.m.p
SILECS_COMM_PATH=/acc/local/L867/silecs/silecs-communication/DEV
include /acc/local/share/silecs/silecs-makefile/DEV/MakeSILECS.include 
 
COMPILER_FLAGS += $(SILECS_CXXFLAGS)
LINKER_FLAGS += $(SILECS_LIBS)

all: silecs-test-opcua.cpp
	g++ -g -O0 -std=c++11 silecs-test-opcua.cpp -o silecs-test-opcua  $(COMPILER_FLAGS) $(LINKER_FLAGS) -lm -lpthread -L/acc/local/L867/3rdparty/open62541/lib -lopen62541-compat -L/acc/local/L867/3rdparty/libmodbus/3.1.6/lib -lmodbus


