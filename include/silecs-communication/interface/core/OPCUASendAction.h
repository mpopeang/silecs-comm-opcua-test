

#ifdef OPCUA_SUPPORT_ENABLED

#ifndef _SEND_OPCUA_ACTION_H_
#define _SEND_OPCUA_ACTION_H_

#include <silecs-communication/interface/core/PLCAction.h>

#include <uaplatformlayer.h>
#include <uaclient/uaclientsdk.h>
#include <uaclient/uasession.h>

namespace Silecs
{

class Action;
class Block;
class Context;


class OPCUASendBlockMode : public PLCAction
{

public:
    OPCUASendBlockMode(Block* block, const std::string& name);
    virtual ~OPCUASendBlockMode();

    int execute(Context *pContext);

private:
    void errChk(int code);

};


class OPCUASendDeviceMode : public PLCAction
{

public:
    OPCUASendDeviceMode(Block* block, const std::string& name);
    virtual ~OPCUASendDeviceMode();

    int execute(Context *pContext);

private:
    void errChk(int code);

};

} // namespace Silecs

#endif // _SEND_OPCUA_ACTION_H_
#endif // OPCUA_SUPPORT_ENABLED