/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/
#ifndef _SILECS_ACTION_H_
#define _SILECS_ACTION_H_

#include <string>

namespace Silecs
{
class Block;
class Context;

/*!
 * \class Action
 * \brief Action defines the abstract level of any action
 */
class Action
{

public:
    Action(Block* block, const std::string& name) :
                    theBlock_(block),
                    name_(name)
    {
    }
    ;
    virtual ~Action()
    {
    }
    ;

    /*!
     * \fn execute
     * It contains the code to be executed for this action.
     * Has to be overriden for concrete PLC action.
     * \return 0 if no error occurred. else return a error status from low level library
     */
    virtual int execute(Context* pContext) = 0;

protected:
    /// Parent block reference of that action
    Block* theBlock_;

    /// Main attributes of the action
    std::string name_;
};

} // namespace

#endif // _SILECS_ACTION_H_

