/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/
#ifdef NI_SUPPORT_ENABLED
#ifndef _CNV_SEND_ACTION_H_
#define _CNV_SEND_ACTION_H_

#include <silecs-communication/interface/core/PLCAction.h>

namespace Silecs
{
class Action;
class Block;
class Context;

/*!
 * \class CNVSendBlockMode
 * \brief Concrete action responsible to send a data block to the CNV (Block mode configuration)
 */
class CNVSendBlockMode : public PLCAction
{

public:
    CNVSendBlockMode(Block* block, const std::string& name);
    virtual ~CNVSendBlockMode();

    /*!
     * \fn execute
     * Connect the CNV if necessary and send data-block to its memory.
     * Overwrite the abstract method
     * \return 0 if no error occurred. else return a error status from low level library
     */
    int execute(Context* pContext);

private:
    /* generate a string exception out of a CNV error code*/
    void errChk(int code) throw(std::string*);

};

/*!
 * \class CNVSendDeviceMode
 * \brief Concrete action responsible to send a data block to the CNV (Device mode configuration)
 */
class CNVSendDeviceMode : public PLCAction
{

public:
    CNVSendDeviceMode(Block* block, const std::string& name);
    virtual ~CNVSendDeviceMode();

    /*!
     * \fn execute
     * Connect the CNV if necessary and Get data-block to its memory.
     * Overwrite the abstract method
     * \return 0 if no error occurred. else return a error status from low level library
     */
    int execute(Context* pContext);

private:
    /* generate a string exception out of a CNV error code*/
    void errChk(int code) throw(std::string*);

};

} // namespace

#endif // _CNV_SEND_ACTION_H_
#endif //NI_SUPPORT_ENABLED
