/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/
#ifndef _DIAGNOSTIC_H_
#define _DIAGNOSTIC_H_

#include <string>
#include <iostream>

namespace Silecs
{
// Possible state of the FEC/PLC/Class connection
static const std::string ConnectionStatusString[] = {"Connected", "Disconnected", "Undefined"};

typedef enum
{
    Connected,
    Disconnected,
    Undefined
} ConnectionStatus;

static const std::string PLCStatusString[] = {"On", "Off", "Unknown"};

typedef enum
{
    On,
    Off,
    Unknown
} PLCStatus;

/*!
 * \class Status
 * \brief This class is used to manage the PLC connection status
 */
class Status
{
public:
    Status();
    virtual ~Status();

    inline ConnectionStatus getConnectionStatus()
    {
        return connStatus_;
    }
    inline PLCStatus getPLCStatus()
    {
        return plcStatus_;
    }
    inline std::string getConnectionStatusAsString() const
    {
        return ConnectionStatusString[connStatus_];
    }
    inline std::string getPLCStatusAsString() const
    {
        return PLCStatusString[plcStatus_];
    }

private:
    friend class PLC;

    /// Current status of the FEC/PLC/Class connection (Connected/Disconnected)
    ConnectionStatus connStatus_;
    /// Current status of the PLC (on/off)
    PLCStatus plcStatus_;
};

} // namespace

#endif // _DIAGNOSTIC_H_
