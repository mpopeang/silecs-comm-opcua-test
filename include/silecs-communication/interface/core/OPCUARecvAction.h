

#ifdef OPCUA_SUPPORT_ENABLED

#ifndef _RECV_OPCUA_ACTION_H_
#define _RECV_OPCUA_ACTION_H_

#include <silecs-communication/interface/core/PLCAction.h>

#include <uaplatformlayer.h>
#include <uaclient/uaclientsdk.h>
#include <uaclient/uasession.h>


namespace Silecs
{

class Action;
class Block;
class Context;


class OPCUARecvBlockMode : public PLCAction
{

public:
    OPCUARecvBlockMode(Block* block, const std::string& name);
    virtual ~OPCUARecvBlockMode();

    int execute(Context *pContext);

private:
    void errChk(int code);

};


class OPCUARecvDeviceMode : public PLCAction
{

public:
    OPCUARecvDeviceMode(Block* block, const std::string& name);
    virtual ~OPCUARecvDeviceMode();

    int execute(Context* pContext);

private:
    void errChk(int code);

};


} // namespace Silecs


#endif // _RECV_OPCUA_ACTION_H_
#endif // OPCUA_SUPPORT_ENABLED