/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/
#ifndef _PLC_SEND_ACTION_H_
#define _PLC_SEND_ACTION_H_

#include <silecs-communication/interface/core/PLCAction.h>

namespace Silecs
{
class Action;
class Block;
class Context;

/*!
 * \class PLCSendBlockMode
 * \brief Concrete action responsible to send a data block to the PLC (Block mode configuration)
 */
class PLCSendBlockMode : public PLCAction
{

public:
    PLCSendBlockMode(Block* block, const std::string& name);
    virtual ~PLCSendBlockMode();

    /*!
     * \fn execute
     * Connect the PLC if necessary and send data-block to its memory.
     * Overwrite the abstract method
     * \return 0 if no error occurred. else return a error status from low level library
     */
    int execute(Context* pContext);

private:

};

/*!
 * \class PLCSendDeviceMode
 * \brief Concrete action responsible to send a data block to the PLC (Device mode configuration)
 */
class PLCSendDeviceMode : public PLCAction
{

public:
    PLCSendDeviceMode(Block* block, const std::string& name);
    virtual ~PLCSendDeviceMode();

    /*!
     * \fn execute
     * Connect the PLC if necessary and Get data-block to its memory.
     * Overwrite the abstract method
     * \return 0 if no error occurred. else return a error status from low level library
     */
    int execute(Context* pContext);

private:

};

} // namespace

#endif // _PLC_SEND_ACTION_H_

