/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/
#ifndef _SILECS_PLC_BLOCK_H_
#define _SILECS_PLC_BLOCK_H_

#include <silecs-communication/interface/core/SilecsService.h>
#include <silecs-communication/interface/utility/Thread.h>
#include <silecs-communication/interface/equipment/SilecsBlock.h>

namespace Silecs
{

class PLCBlock : public Block
{

public:
    PLCBlock(PLC* thePLC, ElementXML* pDesignEl, AccessType accessType, AccessArea accessArea, std::vector<ElementXML*>* pDesignBlockRegisterElCol);
    ~PLCBlock();

    inline unsigned long& getBufferSize()
    {
        return bufferSize_;
    }

protected:
    friend class Device;

    /// Send/Receive data size (size of block * nb of device)
    unsigned long bufferSize_;

};

/*! -----------------------------------------------------------------------
 * \class InputBlock
 * \brief This is a specific Block for Acquisition data (from PLC to client)
 */
class InputBlock : public PLCBlock
{

public:
    InputBlock(PLC* thePLC, ElementXML* pDesignEl, AccessType accessType, AccessArea accessArea, std::vector<ElementXML*>* pDesignBlockRegisterElCol);
    virtual ~InputBlock();

};

/*! -----------------------------------------------------------------------
 * \class OutputBlock
 * \brief This is a specific Block for Settings (from client to PLC)
 */
class OutputBlock : public PLCBlock
{

public:
    OutputBlock(PLC* thePLC, ElementXML* pDesignEl, AccessType accessType, AccessArea accessArea, std::vector<ElementXML*>* pDesignBlockRegisterElCol);
    virtual ~OutputBlock();

};

} // namespace

#endif // _SILECS_BLOCK_H_
