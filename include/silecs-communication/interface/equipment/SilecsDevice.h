/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/
#ifndef _SILECS_DEVICE_H_
#define _SILECS_DEVICE_H_

#include <silecs-communication/interface/core/SilecsService.h>

#include <string>
#include <vector>
#include <map>

namespace Silecs
{
class PLC;
class Block;
class ElementXML;
class Register;
class Context;

/// @cond
typedef std::vector<std::pair<std::string, Register*> > registerVectorType;
typedef std::map<std::string, std::vector<Register*> > blockRegisterMapType;
/// @endcond

/*!
 * \class Device
 * \brief This object maintains a reference of all the device registers.
 * It is responsible to serialize, de-serialize data for the network transfer (alignment, data swapping, ..).
 * It can be used to trig the data-block exchange for one particular device (make sense for DEVICE-MODE only).
 */
class Device
{
public:

    /*!
     * \brief Returns reference of the PLC where the device is instantiated
     * \return Reference of the PLC object
     */
    PLC* getPLC();

    /*!
     * \brief Returns the label of the device as defined in the Deployment document.
     *  A device label is not visible from the supervision. It's the device identifier within
     *  the PLC/Class scope. If label has not been defined, the device is identified by its
     *  corresponding index [1..n] in the device collection.
     * \return label or index of the device as string
     */
    std::string getLabel();

    /*!
     * \brief Returns one register instance of the device by its name
     * \param registerName name of the register
     * \return Reference of the register object
     */
    Register* getRegister(std::string registerName);

    /*!
     * \brief Acquires one particular registers block of the device.
     * The method tries to (re)connect the PLC if the connection is not established yet or unexpectedly interrupted (network failure, PLC down, etc.).
     * It generates an Silecs::SilecsException if the related PLC has not been enabled (using the connect() method).
     * \param blockName name of the block to be acquired
     * \return 0 if operation was successful else an error code (when available).
     */
    int recv(std::string blockName);

    /*!
     * \brief Transmits the registers block to the device
     * The method tries to (re)connect the PLC if the connection is not established yet or unexpectedly interrupted (network failure, PLC down, etc.).
     * It generates an Silecs::SilecsException if the related PLC has not been enabled (using the connect() method).
     * \param blockName name of the block to be transmitted
     * \return 0 if operation was successful else an error code (when available).
     */
    int send(std::string blockName);

    /*!
     * \brief Copies input buffer in output buffer
     * The method copies the input buffer in the output buffer for each READ-WRITE register for the block passed as parameter
     * \param blockName name of the block for which the copy must be performed
     */
    void copyInToOut(const std::string blockName);

    /// @cond
    /*!
     * \return all registers of the device for a given block
     */
    std::vector<Register*>& getRegisterCollection(std::string blockName);

    /*!
     * \return String list (space separated value) of the Device registers
     */
    std::string getRegisterList();
    /// @endcond

private:
    // export device attributes to the Actions
    friend class PLC;
    friend class Register;
    friend class PLCRegister;
    friend class PLCRecvBlockMode;
    friend class PLCRecvDeviceMode;
    friend class PLCSendBlockMode;
    friend class PLCSendDeviceMode;
    friend class Context;

    friend class CNVRegister;
    friend class CNVRecvBlockMode;
    friend class CNVRecvDeviceMode;
    friend class CNVSendBlockMode;
    friend class CNVSendDeviceMode;
    friend class OPCUARegister;
    friend class OPCUARecvDeviceMode;
    friend class OPCUASendDeviceMode;

    Device(PLC* thePLC, ElementXML* pEl, std::vector<ElementXML*>* designRegisterCol);
    virtual ~Device();

    inline long& getInputAddress(AccessArea area)
    {
        if (area == Digital)
            return di_address_;
        else if (area == Analog)
            return ai_address_;
        return address_;
    }

    inline long& getOutputAddress(AccessArea area)
    {
        if (area == Digital)
            return do_address_;
        else if (area == Analog)
            return ao_address_;
        return address_;
    }

    /*!
     * \fn instantiateRegisters
     * \brief Parse the Silecs configuration of that device instantiate related registers.
     */
    void instantiateRegisters(std::vector<ElementXML*>* designRegisterCol);

    /*!
     * \fn setBlockRegisterMap
     * \brief Used to sort the registers per related Block name
     */
    void setBlockRegisterMap(std::string blockName, AccessType accessType, AccessArea accessArea, std::vector<ElementXML*>* pDesignBlockRegisterElCol);

    /*!
     * \fn keepMasterRegisters
     * \brief This method is used for Retentive registers synchronization on each PLC (re)connection.
     * Update local value of the RW Master registers to maintain the PLC values during
     * the synchronization of Slave registers.
     */
    void keepMasterRegisters(std::string blockName);

    /*!
     * \fn getBlock
     * \brief returns one instance of the requested block checking its access-type
     */
    Block* getBlock(const std::string blockName, AccessType accessType = InOut);

    /*!
     * \fn importRegisters
     * \brief Extract all the concerned registers data of that device from a given block and set the registers value.
     * \param pBlock: block which contain list of registers to be imported
     * \param pBuffer: direct pointer on the buffer which contain the registers data
     * \param ts is the time-of-day to time-stamp each register
     */
    void importRegisters(Block* pBlock, void* pBuffer, timeval ts, Context* pContext);

    /*!
     * \fn exportRegisters
     * \brief Export all the concerned registers value of that device to a given block.
     * \param pBlock: block which contain list of registers to be exported
     * \param pBuffer: direct pointer on the buffer which will contain the registers data
     */
    void exportRegisters(Block* pBlock, void* pBuffer, Context* pContext);

    /// Parent PLC reference of that device
    PLC* thePLC_;

    /// Device attributes
    std::string label_;
    long address_;
    long ai_address_;
    long ao_address_;
    long di_address_;
    long do_address_;

    /// Register collection of that device
    registerVectorType registerCol_;

    /// Collections of the device registers by block
    blockRegisterMapType blockRegisterMap_;

};

} // namespace

#endif // _SILECS_DEVICE_H_
