

#ifdef OPCUA_SUPPORT_ENABLED
#ifndef _SILECS_OPCUA_BLOCK_H_
#define _SILECS_OPCUA_BLOCK_H_

#include <silecs-communication/interface/equipment/SilecsBlock.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>

#include <silecs-communication/interface/utility/XMLParser.h>
#include <silecs-communication/interface/utility/SilecsLog.h>

#include <uaplatformlayer.h>
#include <uaclient/uaclientsdk.h>
#include <uaclient/uasession.h>

namespace Silecs
{

class OPCUABlock : public Block
{

public:
    OPCUABlock(PLC *thePLC, ElementXML *pDesignEl, AccessType accessType, AccessArea accessArea, std::vector<ElementXML*>* pDesignBlockRegisterElCol);
    ~OPCUABlock();

    std::vector<UaString> getRegisterNames();
    size_t getNumberOfRegisters();

protected:
    friend class Device;

    void errChk(int code);
    // Will store register names to instantiate nodes when an action is made
    std::vector<UaString> registerNames;
    UaDataValues buffer_;
    size_t numberOfRegisters;

};


class OPCUAInputBlock : public OPCUABlock
{

public:
    OPCUAInputBlock(PLC *thePLC, ElementXML *pDesignEl, AccessType accessType, AccessArea accessArea, std::vector<ElementXML*>* pDesignBlockRegisterElCol);
    ~OPCUAInputBlock();

protected:
    // UaReadValueIds nodeIds;

};


class OPCUAOutputBlock : public OPCUABlock
{

public:
    OPCUAOutputBlock(PLC *thePLC, ElementXML *pDesignEl, AccessType accessType, AccessArea accessArea, std::vector<ElementXML*>* pDesignBlockRegisterElCol);
    ~OPCUAOutputBlock();

protected:
    // UaWriteValues nodeIds;

};  


} // namespace Silecs

#endif // _SILECS_OPCUA_BLOCK_H_
#endif // OPCUA_SUPPORT_ENABLED