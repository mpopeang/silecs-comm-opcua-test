/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/
#ifndef _SILECS_CLUSTER_H_
#define _SILECS_CLUSTER_H_

#include <silecs-communication/interface/core/SilecsService.h>
#include <silecs-communication/interface/utility/Thread.h>
#include <map>
#include <vector>
#include <string>

namespace Silecs
{
class Cluster;
class PLC;
class Device;
class Block;
class WrapperAction;

/// @cond
typedef std::map<std::string, std::vector<Task<WrapperAction> >*> taskMapType;
/// @endcond
typedef std::map<std::string, PLC*> plcMapType;

/*!
 * \class ClusterConfig
 * \brief Run-time configuration of the cluster.
 * The default parameters of the cluster can be changed by setting the public Cluster::configuration structure.
 * \param sharedConnection = true to share the same connection with other clusters deployed on that PLC.
 * By default the sharedConnection of the cluster is false, and it uses a dedicated connection (2 channels: r/w)
 * to communicate with the controller.
 * Setting the configuration parameters must be done at first just after getting the cluster from the service.
 */
class ClusterConfig
{
public:
    ClusterConfig() :
                    sharedConnection(false)
    {
    }
    ;
    ClusterConfig(bool sharedConnectionFlag) :
                    sharedConnection(sharedConnectionFlag)
    {
    }
    ;

    bool getSharedConnection() const;
    void setSharedConnection(bool sharedConnectionFlag);

    //private: direct access of the variable is deprecated - should be private from next MAJOR release (use set/get only)
    bool sharedConnection; //default: not shared (1 dedicated connection (r+w) per class per PLC.
};

//deprecated type - User should use directly Silecs::ClusterConfig class
typedef ClusterConfig clusterConfigType;

/*!
 * \class Cluster
 * \brief An SILECS configuration is defined by a class name and a version number.
 * A Cluster is a tree that defines the set of components for a given configuration.
 * It's the root component of the tree that is used to instantiate each PLC object
 * in which is deployed the given class/version configuration.
 * Finally, the Cluster is the entry point to create and clean-up all the SILECS resources
 * within the process.
 * It can be used to trig the data-block exchanges for all PLCs of the Cluster at the same time.
 */
class Cluster
{

public:
    /*!
     * \brief Returns the network name of the client process host
     * \return client host-name
     */
    std::string getHostName();

    /*!
     * \brief Returns the name of the SILECS class attached to the Cluster
     * \return class name
     */
    std::string getClassName();

    /*!
     * \brief Returns the version number of the SILECS class attached to the Cluster
     * \return class version
     */
    std::string getClassVersion();

    /*!
     * \brief Provides the list of Blocks defined in the corresponding Class/Version.
     * At least one PLC must be attached (connected) to the Cluster, otherwise getBlockList()
     * returns an empty string.
     * \param select access-type of the blocks (Input, Output, InOut)
     * \return String list (space separated value) of Blocks name that have selected access-type
     */
    std::string getBlockList(AccessType accessType);

    /*!
     * \brief Provides the list of Blocks defined in the corresponding Class/Version.
     * At least one PLC must be attached (connected) to the Cluster, otherwise getBlockList()
     * returns an empty string.
     * \return String list (space separated value) of all Block names
     */
    std::string getBlockList();

    /*!
     * \brief Returns a PLC instance of the Cluster, by its name or IP-address
     * Instantiates a new PLC object if required, on the first call.
     * The related class/version must be deployed in that PLC (else generate an Exception).
     * The PLC connection is disabled for the time being.
     * \param plcID host-name or IP-address of the PLC
     * \return Reference of the PLC object
     */
    PLC* getPLC(std::string plcID);

    /*!
     * \brief Provides the list of the PLCs attached to the Cluster (using getPLC() method)
     * \return String list (space separated value) of PLCs name
     */
    std::string getPLCList();

    /*!
     * \brief Provides the  map of the PLC objects attached to the Cluster (using getPLC() method)
     * \return PLC object map
     */
    const plcMapType& getPLCMap();

    /*!
     * \brief Acquires one particular registers block of all devices of all enabled PLCs of that cluster.
     * The method tries to (re)connect each PLC if the connection is not established yet or unexpectedly interrupted (network failure, PLC down, etc.).
     * \param blockName name of the block to be acquired
     */
    void recv(std::string blockName);

    /*!
     * \brief Transmits the registers block to all devices of all enabled PLCs of that cluster.
     * The method tries to (re)connect each PLC if the connection is not established yet or unexpectedly interrupted (network failure, PLC down, etc.).
     * \param blockName name of the block to be transmitted
     */
    void send(std::string blockName);

    /*!
     * \brief Copies input buffer in output buffer
     * The method copies the input buffer in the output buffer for each READ-WRITE register register in the cluster for the block passed as parameter
     * \param blockName name of the block for which the copy must be performed
     */
    void copyInToOut(const std::string blockName);

    /*!
     * \struct configuration
     * \brief Run-time configuration of the cluster.
     * The default parameters of the cluster can be changed by setting the public Cluster::configuration structure.
     * \param sharedConnection = true to share the same connection with other clusters deployed on that PLC.
     * By default the sharedConnection of the cluster is false, and it uses a dedicated connection (2 channels: r/w)
     * to communicate with the controller.
     */
    clusterConfigType configuration;

protected:
    inline void plcLock()
    {
        plcMux_->lock();
    }
    inline void plcUnlock()
    {
        plcMux_->unlock();
    }

private:
    friend class Service;
    friend class PLC;
    friend class Device;

    // private, because this class is a unique per className
    Cluster(const std::string className, const std::string classVersion);

    /*!
     * \brief The client process is responsible to delete the Cluster object that has been
     * instantiated with getCluster() call.
     * Deleting the Cluster object delete also all the related components of that Cluster
     * (PLCs, Devices and Registers)
     */
    ~Cluster();

    bool getHostNameByAddress(const std::string IPAddress, std::string& hostName);
    bool getAddressByHostName(const std::string hostName, std::string& IPAddress);

    /*!
     * \fn getBlock
     * \brief returns one instance of the requested block checking its access-type
     */
    Block* getBlock(const std::string blockName, AccessType accessType = InOut);

    /// Store the current FEC name
    std::string hostName_;

    /// Class related to the cluster
    std::string className_;
    std::string classVersion_;

    /// PLC collection of the cluster
    plcMapType plcMap_; //by plc hostname
    plcMapType IPAddrMap_; //by plc IP address
    Mutex* plcMux_; //Mutex used to protect the find/insert actions of the PLC's map

};

} // namespace

#endif // _SILECS_CLUSTER_H_
