

#ifdef OPCUA_SUPPORT_ENABLED
#ifndef _SILECS_OPCUA_REGISTER_H_
#define _SILECS_OPCUA_REGISTER_H_

#include <silecs-communication/interface/equipment/SilecsRegister.h>
#include <silecs-communication/interface/equipment/SilecsDevice.h>
#include <silecs-communication/interface/utility/XMLParser.h>
#include <silecs-communication/interface/utility/SilecsLog.h>

#include <uaplatformlayer.h>
#include <uaclient/uaclientsdk.h>
#include <uaclient/uasession.h>

namespace Silecs
{

class OPCUARegister : public Register
{

public:
    OPCUARegister(Device* theDevice, ElementXML* pDesignEl);
    ~OPCUARegister();

    int getIndexInBlock();


protected:
    friend class PLC;
    friend class Device;

    void importValue(void* pBuffer, timeval ts);
    void exportValue(void* pBuffer);

    void importString(void* pBuffer, timeval ts);
    void exportString(void* pBuffer);

    OpcUaType getOpcuaType();

    void copyValue();

private:
    // will not need special getters, setters at least for scalar values I think, opcua types should correspond to standard types
    OpcUaType opcuaType;
    UaVariant recvValue;
    UaVariant sendValue;

    int getIndexOfRegister();

};

} // namespace Silecs

#endif // _SILECS_OPCUA_REGISTER_H_
#endif // OPCUA_SUPPORT_ENABLED
