/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/
#ifndef _IETYPE_H_
#define _IETYPE_H_

/* ---------------------------------------------------------*/
/* INCLUDE FILES																						*/
/* ---------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <errno.h>
#ifdef __Lynx__
#include <time.h>
#else
#include <sys/time.h>
#endif
#include <pthread.h>

/* ---------------------------------------------------------*/
/* LOCAL CONSTANT DEFINITIONS                               */
/* ---------------------------------------------------------*/

/* protocol parameters */
/*v1.1 modif.
 Create individual constant for defer cmd data in order to
 allow large command for fetch/write & modbus protocol.
 */
/*v1.1 modif. (previous one, still valid)
 DATA_CMD_DEFER_SIZE: 8192 --> 4096 (RFC) to support deferred 
 mode with S7-300 PLC. This modification was necessary to avoid 
 S7-341 ethernet coupler crashing when receiving to big command
 defer frame.
 Size = 8192 had been choosen for S7-400 ISOLDE project but is too
 large for S7-300 memory. 
 Attention! This v1.1 modification double the number of 
 deferred transation with pisobeam if we recompile 'plcgenrt' process
 with this new version (normally we woudln't have to do it)
 */
#define MAX_RFC_CMD_DEFER_SIZE   4096  /*byte - see PLC code      				 */
#define MAX_FEW_CMD_DEFER_SIZE   16384 /*byte - no constraint on PLC side		 */
#define MAX_MDB_CMD_DEFER_SIZE   16384 /*byte - no constraint on PLC side		 */
#define MAX_S7_CMD_DEFER_SIZE    16384 /*byte - no constraint on PLC side		 */
#define MAX_DATA_CMD_DEFER_SIZE  16384 /*byte - MUST BE the max 3 above constants*/

#define MAX_DATA_REP_DEFER_SIZE  65536 /*v1.3, byte - see PLC code*/
#define MAX_DATA_REP_ASYNC_SIZE  4096  /*byte - see PLC code*/

/* error constant */
// silecs internal error ...................................
// don't forget to modify IeGetErrorMessage(..) function
#define RFC_SYS_ERROR			(int)-1
#define RFC_PARAM_ERROR			(int)-2
#define RFC_CONNECT_ERROR		(int)-3
#define RFC_DISCONNECT_ERROR	(int)-4
#define RFC_TIMEOUT_ERROR		(int)-5
#define RFC_EPIPE_ERROR			(int)-6
#define RFC_FRAME_ERROR			(int)-7

#define IE_SYS_ERROR              RFC_SYS_ERROR
#define IE_PARAM_ERROR            RFC_PARAM_ERROR
#define IE_CONNECT_ERROR          RFC_CONNECT_ERROR
#define IE_DISCONNECT_ERROR       RFC_DISCONNECT_ERROR
#define IE_TIMEOUT_ERROR          RFC_TIMEOUT_ERROR
#define IE_EPIPE_ERROR            RFC_EPIPE_ERROR
#define IE_FRAME_ERROR            RFC_FRAME_ERROR
#define IE_DEFER_CMD_BUFFER_FULL  (int)-100
#define IE_DEFER_REP_BUFFER_EMPTY (int)-101
#define IE_NB_MONITORING_ERROR    (int)-102
#define IE_HOST_PING_ERROR        (int)-103
#define IE_CONFIG_FILE_ERROR      (int)-104
#define IE_BLOCK_SIZE_ERROR       (int)-105
#define IE_HOST_UNKNOWN_ERROR     (int)-106

// PLC report error .......................................
#define IE_EQUIPMENT_UNKNOWN_ERROR (int)-200 // Class/Eqp unknow
#define IE_MEMORY_OVERFLOW_ERROR   (int)-201 // get data buffer too long
#define IE_ACCESS_VIOLATION_ERROR  (int)-202 // DB unknown or get/set access violation
#define IE_MD_MONITORING_ERROR     (int)-203 // bad monitoring desc. or already used
#define IE_ALLOC_MONITORING_ERROR  (int)-204 // monitoring record allocation failed
#define IE_UNALLOC_MONITORING_ERROR (int)-205 // monitoring record unallocation failed
#define IE_SET_TIME_IOCTL_ERROR     (int)-206 // PLC date_time SET failed
#define IE_KEEP_ALIVE_IOCTL_ERROR   (int)-207 // PLC keep-alive transaction failed

/* protocol parameters */
#define MAX_CONNECT_TIMEOUT					2  /*s  */
#define MAX_DATA_TIMEOUT					12 /*s  v1.3*/

/* ---------------------------------------------------------*/
/* MACRO DEFINITIONS					  													  */
/* ---------------------------------------------------------*/

// Used to convert DT PLC format to/from DSC time format
// see convert functions below
#define _frombcd(a) (int)(((a>>4)*10)+(a&0x0F))
#define _tobcd(a)   (((unsigned char)((a)/10)<<4)+((a)%10))

/* ---------------------------------------------------------*/
/* TYPE DEFINITIONS                                         */
/* ---------------------------------------------------------*/

/* PLCs data format ........................................*/
typedef char _CHAR;
typedef unsigned char _BYTE;
typedef unsigned short _WORD;
typedef unsigned long _DWORD;
typedef short _INT;
typedef long _DINT;
typedef float _REAL;
typedef unsigned char _DT[8];

#endif /* _IETYPE_H_ */
