
#ifdef OPCUA_SUPPORT_ENABLED

#ifndef _OPCUA_CONNECTION_H_
#define _OPCUA_CONNECTION_H_

#include <silecs-communication/interface/communication/SilecsConnection.h>
#include <uaplatformlayer.h>
#include <uaclient/uaclientsdk.h>
#include <uaclient/uasession.h>

namespace Silecs
{

class OPCUAConnection : public Connection
{
public:
    OPCUAConnection(PLC* thePLC);
    virtual ~OPCUAConnection();


    /* Virtual member functions from superclass*/
    // OPCUA doesn't use addresses, implemented here because it is virtual in super class
    int readData(PLC *thePLC, long address, unsigned long offset, unsigned long size, unsigned char* buffer)
    {
        return -1;
    }

    int writeData(PLC *thePLC, long address, unsigned long offset, unsigned long size, unsigned char* bugger)
    {
        return -1;
    }

    int readMemory(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    int writeMemory(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    int readAIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    int writeAIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    int readDIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    int writeDIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);

    // Read data from a node with the passed handle
    int readData(PLC *thePLC, UaReadValueIds, UaDataValues &output);
    int writeData(PLC *thePLC, UaWriteValues handle);

    bool isSessionOn();

private:
    bool sessionOn = false;
    int namespaceId = 1;

    UaClientSdk::UaSession session;
    UaClientSdk::SessionConnectInfo sessionInfo;
    UaClientSdk::SessionSecurityInfo securityInfo;

    bool open(PLC *thePLC);
    bool close(PLC *thePLC);

    uint16_t getTCPPort() const;

    bool checkError(PLC *thePLC, int err, bool retry) {return false;}

};

} //namespace

#endif // _OPCUA_CONNECTION_H_
#endif // OPCUA_SUPPORT_ENABLED