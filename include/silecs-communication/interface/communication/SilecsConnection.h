/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/
#ifndef _SILECS_CONNECTION_H_
#define _SILECS_CONNECTION_H_

#include <silecs-communication/interface/equipment/PLCRegister.h>
#include <silecs-communication/interface/utility/Mutex.h>
#include <silecs-communication/interface/utility/TimeStamp.h>


namespace Silecs
{
class PLC;

// Time to wait (second) before next reconnection attempt
typedef enum
{
    UrgentConnection = 2,
    ShortTermConnection = 20,
    LongTermConnection = 60
} ConnectionDelay;

#ifdef __x86_64__
typedef long ChannelID; //pointer is 64bits word
#else
typedef int ChannelID; //pointer is 32bits word
#endif

class Connection
{

public:
    Connection(PLC* thePLC);
    virtual ~Connection();

    virtual int readMemory(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* buffer) = 0;

    virtual int writeMemory(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* buffer) = 0;

    virtual int readAIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* buffer) = 0;

    virtual int writeAIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* buffer) = 0;

    virtual int readDIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* buffer) = 0;

    virtual int writeDIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* buffer) = 0;

    /*!
     * \fn enable/disable
     * \brief The client can suspend the data transmission by disabling the connection if required.
     * enable/disable methods are used from the high-level Cluster API: connect/disconnect.
     * \param connectNow can be used to force immediate connection (true)
     */
    virtual bool enable(PLC* thePLC, bool connectNow);
    void disable(PLC* thePLC);
    bool isEnabled();

    static inline bool isAlive()
    {
        return isAlive_;
    }
    bool isConnected();

    virtual int readUnitCode(PLC* thePLC, UnitCodeType& dataStruct);
    virtual int readUnitStatus(PLC* thePLC, UnitStatusType& dataStruct);
    virtual int readCPUInfo(PLC* thePLC, CPUInfoType& dataStruct);
    virtual int readCPInfo(PLC* thePLC, CPInfoType& dataStruct);

    // Return the plc status
    virtual PlcStatus getPlcStatus(PLC* thePLC);

protected:
    friend class Cluster;
    friend class PLC;
    friend class CNVRecvDeviceMode;
    friend class CNVSendDeviceMode;
    friend class OPCUARecvDeviceMode;
    friend class OPCUASendDeviceMode;

    /*!
     * \fn doOpen
     * \brief This method check the current state of the connection and open it if required.
     * doOpen that is called on each transaction is reponsible of the automatic reconnection mechanism.
     */
    virtual bool doOpen(PLC* thePLC);
    void doClose(PLC* thePLC, bool withLock);
    bool reOpen(PLC* thePLC);

    virtual bool open(PLC* thePLC) = 0; //open the connection for a particular PLC brand
    virtual bool close(PLC* thePLC) = 0; //close the connection for a particular PLC brand

    /*!
     * Returns the TCP port used by the protocol.
     */
    virtual uint16_t getTCPPort() const = 0;

    /*!
     * \fn checkError
     * \brief This method attempts to reconnect the PLC in case of "expected" transaction failure.
     * If the returned value is true the request must be repeated.
     * The retry flag defines whether the transaction will be repeated or not
     */
    virtual bool checkError(PLC* thePLC, int err, bool retry) = 0;

    /*!
     * \fn updatePLCStatus
     * \brief Responsible to update the diagnostic variables each time PLC states have changed
     */
    void updateStatus(PLC* thePLC);

    void logError(PLC* thePLC, bool isReachable);

    /*!
     * \fn isTimeToReconnect
     * \brief In order to not overload the network the reconnection attempts are gradually slowed
     * down from second scale to several minutes. This method checks the elapsed time since connection.
     * \return true if it's time to try to reconnect (time unit is second).
     */
    bool isTimeToReconnect();

    // flag used to enable/disable the transactions independently from the scheduling
    bool isEnabled_;

    /* . read-channel and write channel are independent and can be accessed in parallel.
     * . Each channel must be protected against respective concurrent access.
     * . The global action (open,close,etc.) must be protected from any concurrent access.
     */
    Mutex* readMux_; //Mutex used to protect the PLC read-channel resource
    Mutex* writeMux_; //Mutex used to protect the PLC write-channel resource
    Mutex* connMux_; //Mutex used to protect the global PLC connection resource (for open/close, etc.)

    // Time counter to manage the automatic reconnection
    TsCounter* timeConn_;
    ConnectionDelay delayConn_; //current delay between two connection
    unsigned long remainConn_; //number of attempt before next slow-down
    static const unsigned int numberConn_; //number of connection attempt for each connection delay

    /* ping function is a function used to check
     * if PLC is ON (~ping) before trying to connect it.
     */
    /*----------------------------------------------------------*/
    /* This is a function used to connect an host on a port.
     * 'ip' is the ip address string
     * 'port' is the port number (102 for rfc1006 server)
     * 'dst' is the TSAP destination string (exp: "TCP-1")
     * 'src' is the TSAP source string (exp: "TCP-1")
     * 'ts' timeout in second (1s minimum)
     * return a socket descriptor or <0 on error
     * (see constant error)
     *
     * Details:
     * intermediate connect_nonb() function is used to perform a non-blockant connection.
     * intermediate tcpConnectPing() function is used to check if PLC is ON
     */
    static int tcpPing(const std::string& hostName, const std::string& plcIP, uint16_t port);

    // Communication Diagnostic & Monitoring
    static bool isAlive_; // PLC has repliyed to the ping: true/false
    bool isConnected_; // State of this particular connection: FEC/PLC/Class

    // not copyable object
    Connection(const Connection&);
    Connection& operator=(const Connection&);
};

} // end namespace

#endif // _SILECS_CONNECTION_H_
