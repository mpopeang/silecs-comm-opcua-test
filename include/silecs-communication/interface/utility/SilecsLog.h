/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/
#ifndef _SILECS_LOG_H_
#define _SILECS_LOG_H_

#include <iostream>
#include <sstream>
#include <string>
#include <stdio.h>
#include <iomanip>

namespace Silecs
{
class TsCounter;

#define NO_TOPIC 0	//used to call LOG without filtering: LOG(NO_TOPIC)
#define LOG(topic) if (topic & (Log::topics_ | Log::systopics_)) Log(topic).getLog()
#define LOG_DELAY(topic) if (topic & (Log::topics_ | Log::systopics_)) Log(topic).getLogDelay()
#define TRACE(source) if (TRACE & Log::systopics_) Log(TRACE).getTrace(source)

//Different topics can be used to filter the log messages to print on the std. output (console)
//In addition, 'DIAG' messages are sent to the syslog for post-diagnostic to ease tracking of
//unexpected process behavior (general messages to trace the process sequencing (not ERRORs)).
//By default, console logging is disabled for all topics but can be enable using '-plcLog' arguments.
//The DIAG console output can be enable/disable as well while DIAG syslog output is mandatory.
enum LogTopic
{
    TRACE = 1 << 0, //special topic sent to the ACET tracing service
    DIAG = 1 << 1, //special topic sent to the console (if enable) and to the syslog (everytime)
    ERROR = 1 << 2, //std. topic to trace the SILECS processing errors, sent to the console (if enable)
    DEBUG = 1 << 3, //std. topic to trace the SILECS debug info, sent to the console (if enable)
    SETUP = 1 << 4, //std. topic to trace details of SILECS start-up, sent to the console (if enable)
    ALLOC = 1 << 5, //std. topic to trace allocation memory, sent to the console (if enable)
    RECV = 1 << 6, //std. topic to trace transaction from PLC, sent to the console (if enable)
    SEND = 1 << 7, //std. topic to trace transaction toward PLC, sent to the console (if enable)
    COMM = 1 << 8, //std. topic to trace PLC communication stuff, sent to the console (if enable)
    DATA = 1 << 9, //std. topic to trace transaction details (need SEND/RECV), sent to the console (if enable and )
    LOCK = 1 << 10, //std. topic to trace resources lock details, sent to the console (if enable and )
};

const int topicNb = 11;
const std::string topicList[topicNb] = {"TRACE", "DIAG", "ERROR", "DEBUG", "SETUP", "ALLOC", "RECV", "SEND", "COMM", "DATA", "LOCK", };

class Log
{
public:
    Log(unsigned long topic);
    virtual ~Log();

    std::ostringstream& getLog();
    std::ostringstream& getLogDelay();
    std::ostringstream& getTrace(std::string source);

    // set log arguments: '-plcLog topic1[,topic2,..]'
    static bool setLogArguments(int argc, char ** argv);

    /*!
     * \fn startSyslog
     * \brief Used to enable the standard syslog mechanism
     * \parameter topics: bit-set of the topics to be stored using the syslog (in addition to mandatory TRACE and DIAG)
     */
    static void startSyslog(char* ident, unsigned long systopics);
    static void stopSyslog();

    static std::string& getHost()
    {
        return host_;
    }
    static pid_t& getPid()
    {
        return pid_;
    }
    static std::string& getProcess()
    {
        return process_;
    }

    // return valid log arguments as string: '-plcLog topic1[,topic2,..]'
    static std::string getLogArguments();

    static std::string setTopicToString(unsigned long topic);

    static bool getTopicsFromString(const std::string& topicArgv);

    unsigned long topic_; //current topic of the logger
    static unsigned long topics_; //enabled topics (coming from the process arguments)
    static unsigned long systopics_; //set of topics to be logged using the syslog mechanism

private:
    friend class SilecsService;

    Log(const Log&);
    Log& operator =(const Log&);
    std::ostringstream os;
    static TsCounter timeStamp_;
    static bool syslogIsStarted_;
    static std::string host_;
    static pid_t pid_;
    static std::string process_;

};

} // namespace

#endif //_SILECS_LOG_H_

