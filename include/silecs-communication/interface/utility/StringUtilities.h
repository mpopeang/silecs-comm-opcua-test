/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/
#ifndef _STRING_UTILITIES_H_
#define _STRING_UTILITIES_H_

#include <string>
#include <vector>

namespace Silecs
{

class StringUtilities
{
public:
    static void tokenize(const std::string& str, std::vector<std::string>& tokens, const std::string& delimiters = " ");

    static void trimWhiteSpace(std::string& str);

    // define an array of one pointer on an empty string
    static const unsigned int c_emptyStringArraySize;
    static const char* c_emptyStringArray[1];
    // define an empty string
    static const char c_emptyString[];

    static std::string toString(unsigned int data);
    static std::string toString(int data);
    static std::string toString(unsigned long data);
    static std::string toString(long data);
    static std::string toString(long long data);
    static std::string toString(double data);
    static std::string toString(const void * ptr);
    static void toLower(std::string& str);
};

} // namespace

#endif //_STRING_UTILITIES_H_
