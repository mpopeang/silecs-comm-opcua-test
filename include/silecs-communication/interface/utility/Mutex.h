/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/
#ifndef SILECS_MUTEX_H_
#define SILECS_MUTEX_H_

#include <string>
#include <map>

#include <silecs-communication/interface/utility/SilecsException.h>

namespace Silecs
{

class Condition;

class Mutex
{
public:
    Mutex(const std::string& name = "undefined");
    ~Mutex();

    void lock();
    void unlock();

private:
    Mutex(const Mutex& other);
    Mutex& operator=(const Mutex& other);

    friend class Condition;

    // Mutex name if any, used for logging in particular
    std::string name_;

    // Mutex object
    pthread_mutex_t mutex_;

    // Mutex Attributes
    pthread_mutexattr_t attr_;
};

// For automatic mutex-release on code-block exit ( e.g. if exceptions are used )
class Lock
{
public:
    Lock();
    Lock(Mutex* mutex);
    ~Lock();
    void setMutex(Mutex* mutex);

private:
    Lock(const Lock& other);
    Lock& operator=(const Lock& other);

    Mutex* mutex_;
};

} // namespace

#endif /* SILECS_MUTEX_H_ */
