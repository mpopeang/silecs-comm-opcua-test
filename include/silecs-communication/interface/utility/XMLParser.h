/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/
#ifndef _XMLPARSER_H_
#define _XMLPARSER_H_

#include <vector>
#include <iostream>

#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>
#include <libxml/encoding.h>
#include <libxml/xmlwriter.h>

#include <silecs-communication/interface/utility/SilecsException.h>
#include <silecs-communication/interface/utility/SilecsLog.h>

#define MY_ENCODING "UTF-8"

namespace Silecs
{

/*!
 * \class AttributeXML
 * \brief This class represents an XML attribute containing name and value
 */
class AttributeXML
{
public:
    std::string name_;
    std::string value_;

    inline void toString()
    {
        if (!name_.empty())
            std::cout << " " << name_ << "=" << value_;
    }

    inline bool fromUnsigned(unsigned long &val)
    {
        return (sscanf(value_.c_str(), "%lu", &val) == 1);
    }

    inline bool fromSigned(long &val)
    {
        return (sscanf(value_.c_str(), "%li", &val) == 1);
    }

    inline bool fromString(std::string &val)
    {
        val = value_;
        return true;
    }
};

/*!
 * \class ElementXML
 * \brief This class represents an XML element containing name, value, a list of
 * attributes
 * and a list of child XML elements
 */
class ElementXML
{
public:
    std::string name_;
    std::string value_;
    std::vector<AttributeXML *> *attributeList_;
    std::vector<ElementXML *> *childList_;

    unsigned long getUnsignedAttribute(const std::string attribute)
    {
        unsigned long value;
        for (unsigned i = 0; i < attributeList_->size(); i++)
        {
            AttributeXML *pAt = (*attributeList_)[i];
            if (pAt->name_ == attribute)
            {
                if (pAt->fromUnsigned(value))
                    return value;
                throw SilecsException(__FILE__, __LINE__, XML_DATA_TYPE_MISMATCH);
            }
        }
        throw SilecsException(__FILE__, __LINE__, XML_ATTRIBUTE_NOT_FOUND, attribute);
    }

    unsigned long getSignedAttribute(const std::string attribute)
    {
        long value;
        for (unsigned i = 0; i < attributeList_->size(); i++)
        {
            AttributeXML *pAt = (*attributeList_)[i];
            if (pAt->name_ == attribute)
            {
                if (pAt->fromSigned(value))
                    return value;
                throw SilecsException(__FILE__, __LINE__, XML_DATA_TYPE_MISMATCH);
            }
        }
        throw SilecsException(__FILE__, __LINE__, XML_ATTRIBUTE_NOT_FOUND, attribute);
    }

    std::string getStringAttribute(const std::string attribute)
    {
        std::string value;
        for (unsigned i = 0; i < attributeList_->size(); i++)
        {
            AttributeXML *pAt = (*attributeList_)[i];
            if (pAt->name_ == attribute)
            {
                if (pAt->fromString(value))
                    return value;
                throw SilecsException(__FILE__, __LINE__, XML_DATA_TYPE_MISMATCH);
            }
        }
        throw SilecsException(__FILE__, __LINE__, XML_ATTRIBUTE_NOT_FOUND, attribute);
    }

    long getAddressAttribute(const std::string attribute)
    {
        long value;
        for (unsigned i = 0; i < attributeList_->size(); i++)
        {
            AttributeXML *pAt = (*attributeList_)[i];
            if (pAt->name_ == attribute)
            {
                if (pAt->fromSigned(value))
                    return value;
                throw SilecsException(__FILE__, __LINE__, XML_DATA_TYPE_MISMATCH);
            }
        }
        LOG(SETUP) << attribute << " attribute is not defined. Value has been set to -1 by default";
        return -1;
    }

    void toString()
    {
        if (!name_.empty())
            std::cout << "<" << name_;
        // if (!value_.empty()) std::cout << " el-value:" << value_;
        if (attributeList_)
            for (unsigned i = 0; i < attributeList_->size(); i++)
                (*attributeList_)[i]->toString();
        if (!name_.empty())
            std::cout << "/>" << std::endl;
        if (childList_)
            for (unsigned i = 0; i < childList_->size(); i++)
            {
                std::cout << "\n\t";
                (*childList_)[i]->toString();
            }
        if (!name_.empty())
            std::cout << "</" << name_ << ">" << std::endl;
    }

    ~ElementXML()
    {
        if (attributeList_)
        {
            for (unsigned int i = 0; i < attributeList_->size(); i++)
                delete (*attributeList_)[i];
            delete attributeList_;
        }
        if (childList_)
        {
            for (unsigned int i = 0; i < childList_->size(); i++)
                delete (*childList_)[i];
            delete childList_;
        }
    }
};

/*!
 * \class XMLParser
 * \brief This class offers functions to extract information from an XML file.
 * It uses the xPath functions of the library libXML
 */
class XMLParser
{
public:
    static void xmlInit();
    static void xmlCleanup();

    XMLParser(const std::string fileName);
    ~XMLParser();

    /**
     * \fn extractElementWhere
     * \Extracts information from an XML file by searching for the fisrt XML
     * element name
     * where its 'attributeName' has the 'attributeValue'
     */
    ElementXML *extractElementWhere(const std::string &elementName, const std::string &attributeName, const std::string &attributeValue, bool mandatoryElement);

    /**
     * \fn extractElementsFromXPath
     * \Extracts information from an XML file by searching for an xPath
     * expression
     */
    std::vector<ElementXML *> *
    extractElementsFromXPath(const std::string &xpathExpr, bool mandatoryElement);

    /**
     * \fn extractElements
     * \Extracts information from an XML file by searching for a XML element
     * name
     */
    std::vector<ElementXML *> *extractElements(const std::string &elementName, bool mandatoryElement);
    /**
     * \fn extractAttribute
     * \Extracts information from an XML file by searching for a XML attribute
     *  in an XML element
     */
    AttributeXML *extractAttribute(const std::string &elementName, const std::string &attributeName, bool mandatoryAttribute);

    /**
     * \fn getElements
     * \Retrieve information from a root ElementXML by searching for a sub
     * ElementXML
     */
    std::vector<ElementXML *> *getElements(const ElementXML *pElement, const std::string &elementName, bool mandatoryElement);

private:
    /**
     * \fn extractElementsFrom
     * \Extracts information from an XML file by searching for an xPath
     * expression
     */
    void getElementsFrom(const ElementXML *pElement, const std::string &elementName, std::vector<ElementXML *> *pElementXMLList);

    /**
     * \fn fillElement
     * \Fills instance of the class ElementXML with nodes from an XML file
     */
    void fillElement(xmlNodePtr node, ElementXML *pElement);

    /**
     * \fn trimCarrierReturn
     * \Removes carrier return from string
     */
    void trimCarrierReturn(std::string &s);

    std::string fileName_;
};

} // namespace

#endif //_XMLPARSER_H_
