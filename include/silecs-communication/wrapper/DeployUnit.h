/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/
// Copyright CERN 2015
// SilecsWrapper

#ifndef SILECSWRAPER_DEPLOY_UNIT_H_
#define SILECSWRAPER_DEPLOY_UNIT_H_

#include <silecs-communication/interface/core/SilecsService.h>

namespace SilecsWrapper
{

class DeployConfig
{
public:
    DeployConfig();
    DeployConfig(bool automaticConnect);
    bool getAutomaticConnect() const;
    void setAutomaticConnect(bool automaticConnect);

private:
    bool _automaticConnect;

};

class DeployUnit
{
public:
    DeployUnit(const std::string& name, const std::string& version, const std::string& logTopics, const DeployConfig& globalConfig);
    virtual ~DeployUnit();

    void deleteInstance();

    const std::string& getName() const;
    const std::string& getVersion() const;
    Silecs::Service* getService() const;
    DeployConfig& getGlobalConfig();

    void setGlobalConfig(DeployConfig& globalConfig);

protected:
    const std::string _name;
    const std::string _version;
    DeployConfig _globalConfig;
    Silecs::Service* _silecsService;
    //singleton instance
    static DeployUnit* _instance;

private:
    // Non copyable
    DeployUnit(const DeployUnit& other);
    DeployUnit& operator=(const DeployUnit&);

};

} //namespace SilecsWrapper

#endif
