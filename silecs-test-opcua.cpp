
#include <silecs-communication/interface/core/SilecsService.h>
#include <thread>
#include <chrono>
#include <stdlib.h>

int main(int argc, char **argv)
{
    Silecs::Service *pService;
    Silecs::Cluster *pCluster;
    Silecs::PLC *pPlc;

    try
    {
        pService = Silecs::Service::getInstance(argc, argv);
        pCluster = pService->getCluster("SilecsValid", "1.1.0");
        pPlc = pCluster->getPLC("cwe-513-vpl507");
        pPlc->connect(Silecs::NO_SYNCHRO, true);
	//std::this_thread::sleep_for(std::chrono::seconds(10));
	pPlc->recv("readWriteBlock");
	auto buff = pPlc->getDevice("1")->getRegister("b1_ro")->getValUInt32();
	std::cout << "Read value in register b1_ro: " << buff << std::endl;
	auto buff2 = pPlc->getDevice("1")->getRegister("b2_ro")->getValFloat32();
	std::cout << "Read value in register b2_ro: " << buff2 << std::endl;
        auto buff3 = pPlc->getDevice("1")->getRegister("b3_ro")->getValInt64();
	std::cout << "Read value in register b3_ro: " << buff3 << std::endl;
	int32_t data = 42;
	auto buff4 = pPlc->getDevice("1")->getRegister("b4_ro")->getValString();
	std::cout << "Read value in register b4_ro: " << buff4 << std::endl;
	
	auto aux = pPlc->getDevice("2")->getRegister("b1_ro")->getValUInt32();
	std::cout << "Read value in register b1_ro in device 2 is: " << aux << std::endl;	

	int16_t* arr = (int16_t*)malloc(2 * sizeof(int16_t));
	pPlc->getDevice("1")->getRegister("b5_ro")->getValInt16Array(arr, 2);
	std::cout << "Values in register b5_ro: " << std::endl;
	for (int i = 0; i < 2; ++i)
		std::cout << arr[i] << " ; ";	
	std::cout << std::endl;

	std::string* sarr = (std::string*)malloc(2 * sizeof(std::string));
	new (sarr) std::string();
	new (sarr + 1) std::string();
	//if (sarr == NULL)
	//{
	//	std::cout << "Error";
	//}
	pPlc->getDevice("1")->getRegister("b6_ro")->getValStringArray(sarr, 2);
	std:cout << "Values in register b6_ro: " << std::endl;
	for (int i = 0; i < 2; ++i)
		std::cout << sarr[i] << " ; ";
	std::cout << std::endl;
	
	pPlc->getDevice("1")->copyInToOut("readWriteBlock");	

	int16_t* new_arr = (int16_t*)malloc(2 * sizeof(int16_t));
	new_arr[0] = 18;
	new_arr[1] = 19;
	pPlc->getDevice("1")->getRegister("b5_ro")->setValInt16Array(new_arr, 2);
	
	pPlc->getDevice("1")->getRegister("b1_ro")->setValUInt32(data);

	pPlc->send("readWriteBlock");

	pPlc->getDevice("1")->recv("readWriteBlock");
	auto buff5 = pPlc->getDevice("1")->getRegister("b1_ro")->getValUInt32();
	std::cout << "New values is: " << buff5 << std::endl;
	pPlc->getDevice("1")->getRegister("b5_ro")->getValInt16Array(arr, 2);
	std::cout << "Values in register b5_ro: " << std::endl;
        for (int i = 0; i < 2; ++i)
                std::cout << arr[i] << " ; ";
        std::cout << std::endl;
    }
    catch(const Silecs::SilecsException &ex)
    {
        throw Silecs::SilecsException(__FILE__, __LINE__, ex.getMessage());
    }
    pService->deleteInstance();
    

    return 0;
}
